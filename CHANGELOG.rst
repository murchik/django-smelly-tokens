Changelog
=========

0.5.1 (unreleased)
------------------

- Nothing changed yet.


0.5.0 (2022-06-06)
------------------

- Use pathlib for path manipulation.


0.4.6 (2020-09-21)
------------------

- Better directory exclusion.


0.4.5 (2020-09-21)
------------------

- New pkgutil.get_loader fix.


0.4.4 (2018-06-06)
------------------

- #movingtogitlab


0.4 (2017-05-06)
----------------

- Introduced zest.releaser.
- Released to PyPI.
- Documentation.


0.3 (2017-05-02)
----------------

- Python 2 / 3 compatibility.
- Multiple tokens per class.
